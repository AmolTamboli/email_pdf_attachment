//
//  ViewController.swift
//  Email_PDF_Attachment
//
//  Created by Amol Tamboli on 29/06/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit
import MessageUI

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK:- RUN ONLY DEVICE
    @IBAction func btnSendEmail(_ sender: Any) {
        
        //MARK:- If not run in device then excute this code
        guard MFMailComposeViewController.canSendMail() else {
            return
        }
        
        //MARK:- Email Code
        let composer = MFMailComposeViewController()
        composer.mailComposeDelegate = self
        composer.setToRecipients(["amoltamboli09@gmail.com"])
        composer.setSubject("Apple World")
        composer.setCcRecipients(["Enter Which You Want"])
        composer.setBccRecipients(["Enter You Apple Id"])
        composer.setMessageBody("Hello guys nice to see you", isHTML: true)
        
        
        //MARK:- PDF Attachment Code
        if let filepath = Bundle.main.path(forResource: "sample", ofType: "pdf"){
            if let fileData = NSData(contentsOfFile: filepath){
                composer.addAttachmentData(fileData as Data, mimeType: "application/pdf", fileName: "sample.pdf")
            }
        }
        self.present(composer, animated: true, completion: nil)
    }
}

extension ViewController: MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let _ = error{
            controller.dismiss(animated: true, completion: nil)
            
            switch result {
            case .cancelled:
                print("Cancelled")
            case .failed:
                print("Failed")
            case .saved:
                print("Saved")
            case .sent:
                print("Sent")
            }
            controller.dismiss(animated: true, completion: nil)
        }
    }
}


